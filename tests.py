#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from rework_tracker.reminder_manager import reminder_manager, reminder
from rework_tracker.task_manager import task_manager, task
from rework_tracker.user_manager import user_manager, user
from rework_tracker.exceptions import Error
from rework_tracker.storage import storage
import tempfile
import unittest
import shutil
import json
import os


class UnitTests(unittest.TestCase):

    def setUp(self):

        # creating main manager-class objects
        self.__tracker_dir = tempfile.mkdtemp()
        self.info_storage = storage.Storage(self.__tracker_dir + '/', self.__tracker_dir + '/', self.__tracker_dir + '/')
        self.task_manager = task_manager.TaskManager(self.info_storage)
        self.user_manager = user_manager.UserManager(self.info_storage)
        self.reminder_manager = reminder_manager.ReminderManager(self.info_storage)

        # creating test-files
        with open(os.path.join(self.info_storage.reminder_path, 'reminder.json'), 'w') as j_file:
            json.dump([], j_file)
        with open(os.path.join(self.info_storage.task_path, 'tasks.json'), 'w') as j_file:
            json.dump([], j_file)
        with open(os.path.join(self.info_storage.user_path, 'users.json'), 'w') as j_file:
            json.dump([], j_file)

    def tearDown(self):

        # deleting test-files
        shutil.rmtree(self.__tracker_dir)

    def test_create_user(self):

        # creating a test user-object
        test_user = user.User('test_user_name', 'test_password', 'Y')
        self.user_manager.new_user(test_user)

        # result
        content = {
            'name': test_user.name,
            'password': test_user.password,
            'current': test_user.current
            }

        # compare test_user with content
        users_dict = self.info_storage.load_user_by_name(test_user.name, test_user.password)
        self.assertEqual(users_dict, content)

    def test_create_simple_task(self):

        # creating a test simple_task-object
        test_simple_task = task.SimpleTask('test_simple_task_name', 'test_condition', 'test_group', 3, None,
                                          'test_user_name', 'In_progress')
        self.task_manager.new_simple_task(test_simple_task)

        # result
        content = {
                    'name': test_simple_task.name,
                    'condition': test_simple_task.condition,
                    'group': test_simple_task.group,
                    'priority': test_simple_task.priority,
                    'date_of_execution': test_simple_task.date_of_execution,
                    'executor': test_simple_task.executor,
                    'status': test_simple_task.status
                    }

        # compare test simple_task with content
        tasks_dict = self.info_storage.load_task_by_name(test_simple_task.executor, test_simple_task.name)
        self.assertEqual(tasks_dict, content)

    def test_create_combo_task(self):

        # creating a test combo_task-object
        test_combo_task = task.ComboTask('test_combo_task_name', 'test_condition', 'test_group', 3, None,
                                          'test_user_name', 'In_progress', None)
        self.task_manager.new_combo_task(test_combo_task)

        # result
        content = {
                    'name': test_combo_task.name,
                    'condition': test_combo_task.condition,
                    'group': test_combo_task.group,
                    'priority': test_combo_task.priority,
                    'date_of_execution': test_combo_task.date_of_execution,
                    'executor': test_combo_task.executor,
                    'status': test_combo_task.status,
                    'list_of_sub_tasks': test_combo_task.list_of_sub_tasks
                    }

        # compare test combo_task with content
        tasks_dict = self.info_storage.load_task_by_name(test_combo_task.executor, test_combo_task.name)
        self.assertEqual(tasks_dict, content)

    def test_create_redirected_task(self):

        # creating a test redirected_task-object
        test_redirected_task = task.RedirectedTask('test_redirected_task_name', 'test_condition', 'test_group', 3, None,
                                          'test_executor', 'test_user_name', 'In_progress', None)
        self.task_manager.new_redirected_task(test_redirected_task)

        # result
        content = {
                    'name': test_redirected_task.name,
                    'condition': test_redirected_task.condition,
                    'group': test_redirected_task.group,
                    'priority': test_redirected_task.priority,
                    'date_of_execution': test_redirected_task.date_of_execution,
                    'executor': test_redirected_task.executor,
                    'written_by': test_redirected_task.written_by,
                    'status': test_redirected_task.status,
                    'list_of_sub_tasks': test_redirected_task.list_of_sub_tasks
                    }

        # compare test redirected_task with content
        tasks_dict = self.info_storage.load_task_by_name(test_redirected_task.executor, test_redirected_task.name)
        self.assertEqual(tasks_dict, content)

    def test_create_reminder(self):

        # creating a test reminder-object
        test_reminder = reminder.Reminder('test_reminder_name', 'test_condition', 'ED', None, None, 'test_user_name')
        self.reminder_manager.new_reminder(test_reminder)

        # result
        content = {
                    'name': test_reminder.name,
                    'condition': test_reminder.condition,
                    'frequency': test_reminder.frequency,
                    'start_date': test_reminder.start_date,
                    'finish_date': test_reminder.finish_date,
                    'executor': test_reminder.executor
                    }

        # compare test reminder with content
        reminders_dict = self.info_storage.load_reminder_by_name(test_reminder.executor, test_reminder.name)
        self.assertEqual(reminders_dict, content)

    def test_edit_task(self):

        # creating a test task-objects
        test_task_before_editing = task.SimpleTask('test_task_name_b', 'test_condition_b', 'test_group', 3, None,
                                    'test_user_name', 'In_progress')
        test_task_after_editing = task.SimpleTask('test_task_name_a', 'test_condition_a', 'test_group', 3, None,
                                    'test_user_name', 'In_progress')

        # result
        content_after_editing = {
                    'name': test_task_after_editing.name,
                    'condition': test_task_after_editing.condition,
                    'group': test_task_after_editing.group,
                    'priority': test_task_after_editing.priority,
                    'date_of_execution': test_task_after_editing.date_of_execution,
                    'executor': test_task_after_editing.executor,
                    'status': test_task_after_editing.status
                    }

        # create and process
        self.task_manager.new_simple_task(test_task_before_editing)
        self.task_manager.edit_task(test_task_before_editing.executor, test_task_before_editing, test_task_after_editing)

        # compare test task with content
        tasks_dict = self.info_storage.load_task_by_name(test_task_after_editing.executor, test_task_after_editing.name)
        self.assertEqual(tasks_dict, content_after_editing)

    def test_edit_reminder(self):

        # creating a test reminder-objects
        test_reminder_before_editing = reminder.Reminder('test_reminder_name_b', 'test_condition_b', 'ED', None, None,
                                                         'test_user_name')
        test_reminder_after_editing = reminder.Reminder('test_reminder_name_a', 'test_condition_a', 'ED', None, None,
                                                        'test_user_name')

        # result
        content_after_editing = {
                    'name': test_reminder_after_editing.name,
                    'condition': test_reminder_after_editing.condition,
                    'frequency': test_reminder_after_editing.frequency,
                    'start_date': test_reminder_after_editing.start_date,
                    'finish_date': test_reminder_after_editing.finish_date,
                    'executor': test_reminder_after_editing.executor
                    }

        # create and process
        self.reminder_manager.new_reminder(test_reminder_before_editing)
        self.reminder_manager.edit_reminder(test_reminder_before_editing.executor, test_reminder_before_editing,
                                    test_reminder_after_editing)

        # compare test reminder with content
        reminders_dir = self.info_storage.load_reminder_by_name(test_reminder_after_editing.executor,
                                                         test_reminder_after_editing.name)
        self.assertEqual(reminders_dir, content_after_editing)

    def test_delete_task(self):

        # creating a test task-object
        test_task = task.SimpleTask('test_task_name', 'test_condition', 'test_group', 3, None,
                                    'test_user_name', 'In_progress')

        # create and process
        self.task_manager.new_simple_task(test_task)
        self.task_manager.delete_task(test_task.executor, test_task.name)

        # check result
        tasks_dict = self.info_storage.load_task_by_name(test_task.executor, test_task.name)
        self.assertFalse(tasks_dict)

    def test_delete_reminder(self):

        # creating a test reminder-object
        test_reminder = reminder.Reminder('test_reminder_name', 'test_condition', 'ED', None, None, 'test_user_name')

        # create and process
        self.reminder_manager.new_reminder(test_reminder)
        self.reminder_manager.delete_reminder(test_reminder.executor, test_reminder.name)

        # check result
        reminders_dict = self.info_storage.load_reminder_by_name(test_reminder.executor, test_reminder.name)
        self.assertFalse(reminders_dict)

    def test_error_delete_task(self):

        with self.assertRaises(Error):
            self.task_manager.delete_task('test_user_name', 'test_task_name')

    def test_error_delete_reminder(self):

        with self.assertRaises(Error):
            self.reminder_manager.delete_reminder('test_user_name', 'test_task_name')
