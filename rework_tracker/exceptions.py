#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


class Error(Exception):
    pass


class UserError(Error):
    def __init__(self, msg):
        self.msg = msg


class TaskError(Error):
    def __init__(self, msg):
        self.msg = msg


class ConfigError(Error):
    def __init__(self, msg):
        self.msg = msg