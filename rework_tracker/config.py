#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import configparser
import json
import os


def load_config():

    # calls load of configuration file
    config = _load_config_file()

    return config


def _load_config_file():

    # check for configuration files in the default location
    config = configparser.ConfigParser()
    default_config_path = os.path.expanduser('~/rework_tracker/config.ini')

    if os.path.exists(default_config_path):
        config.read(default_config_path)
        return config
    else:

        # if there are no configuration files, they must be created
        return _create_default_config()


def _create_default_config():

    # definition of standard relative paths for working with different types of job_data(tasks, users, etc.)
    default_directory = os.path.expanduser('~/rework_tracker')
    default_config_path = os.path.join(default_directory, 'config')
    default_tasks_path = os.path.join(default_directory, 'tasks')
    default_reminders_path = os.path.join(default_directory, 'reminders')
    default_users_path = os.path.join(default_directory, 'users')
    default_logs_path = os.path.join(default_directory, 'logs')

    def create_main_config():

        # Creating a file that contains all the necessary for work
        config = configparser.ConfigParser()
        config.add_section('Paths')
        config.set('Paths', 'workspace_path', default_directory)
        config.set('Paths', 'tasks_path', default_tasks_path)
        config.set('Paths', 'reminders_path', default_reminders_path)
        config.set('Paths', 'users_path', default_users_path)
        config.set('Paths', 'logs_path', default_logs_path)
        with open(os.path.join(default_config_path, 'config.ini'), 'w') as config_file:
            config.write(config_file)
        return config

    # creation of directories and json-files for data storage, if they have not yet been created
    # definition of the form(dir of lists of dirs) of writing to a file
    if not os.path.exists(default_directory):
        os.mkdir(default_directory)

    if not os.path.exists(default_users_path):
        os.mkdir(default_users_path)
        content = {'Users': list()}
        with open(os.path.join(default_users_path, 'users.json'), 'w') as json_file:
            json.dump(content, json_file)

    if not os.path.exists(default_reminders_path):
        os.mkdir(default_reminders_path)
        content = {'Reminder': list()}
        with open(os.path.join(default_reminders_path, 'reminders.json'), 'w') as json_file:
            json.dump(content, json_file)

    if not os.path.exists(default_tasks_path):
        os.mkdir(default_tasks_path)
        content = {'SimpleTask': list(), 'ComboTask': list(), 'RedirectedTask': list()}
        with open(os.path.join(default_tasks_path, 'tasks.json'), 'w') as json_file:
            json.dump(content, json_file)

    #  if not os.path.exists(default_logs_path):
    #      os.mkdir(default_logs_path)

    if not os.path.exists(default_config_path):
        os.mkdir(default_config_path)

    return create_main_config()
