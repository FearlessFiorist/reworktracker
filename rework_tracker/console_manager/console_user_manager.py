#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from rework_tracker.storage import storage


def check_current_user():

    # check, what user is active now?
    current_user_name = ""

    # load all users from json-file
    users = storage.load_all_users()

    # searching for a necessary user by "current" parameter
    # "Y" ~ "Yes" ~ yes, it is a current user
    for i, user in enumerate(users):
        if user.get('current') == "Y":
            current_user_name = user.name

    # and return it
    return current_user_name

def change_current_user(content):

    # load all users from json-file
    users = storage.load_all_users()

    # searching for a necessary user by name and change its "current" parameter
    # "Y" ~ "Yes" ~ yes, it is a current user
    # "N" ~ "No" ~ no, it's not a current user
    for i, user in enumerate(users):
        if user.get('name') == content.get('name'):
            user.current = "Y"
        else:
            user.current = "N"

    # save updated list of users
    storage.save_users(users)
