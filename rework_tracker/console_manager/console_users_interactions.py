#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from rework_tracker.reminder_manager import reminder
from rework_tracker.task_manager import task


def incremental_creating(stage, jobs_type):

    # communicate with user for printing and inputting information or give the other functions users choice
    # while user try to create something new(task or reminder)

    # stage is a variable, which explains what stage of the long process is now required to provide the user

    # stage one is choosing what user want to create: task or reminder
    if stage == 1:

        print("Pls, enter what type of job do you want to create"
              "('t' ~ task, 'r' ~ reminder)")
        users_choice = input()

        return users_choice

    # stage two is choosing what type of task user want to create: simple, combo or redirected
    if stage == 2:

        print("Pls, enter what type of task do you want to create"
              "('s' ~ simple(without sub_tasks), 'c' ~ combo, 'r' ~ redirected(task for other user))")
        users_choice = input()

        return users_choice

    # stage three: input parameters of new task
    if stage == 3:

        print("Pls, enter parameters of job that you are creating")
        print("")

        if jobs_type == "simple_task" or jobs_type == "combo_task":

            print("Pls, enter name of the job"
                  "(short name, it'll used for interactions)")
            jobs_name = input()

            print("Pls, enter condition of the job"
                  "(detailed explanation)")
            jobs_condition = input()

            print("Pls, enter group of the job"
                  "('Personal', 'Work', etc)")
            jobs_group = input()

            print("Pls, enter priority of the job"
                  "('1' ~ high, '2' ~ medium, '3' ~ low)")
            jobs_priority = input()

            print("Pls, enter date, when this job may be finished"
                  "(day, month, year)")
            jobs_execution_date = input()

            return jobs_name, jobs_condition, jobs_group, jobs_priority, jobs_execution_date

        elif jobs_type == "redirected_task":

            print("Pls, enter name of the job"
                  "(short name, it'll used for interactions)")
            jobs_name = input()

            print("Pls, enter condition of the job"
                  "(detailed explanation)")
            jobs_condition = input()

            print("Pls, enter group of the job"
                  "('Personal', 'Work', etc)")
            jobs_group = input()

            print("Pls, enter priority of the job"
                  "('1' ~ high, '2' ~ medium, '3' ~ low)")
            jobs_priority = input()

            print("Pls, enter date, when this job may be finished"
                  "(day, month, year)")
            jobs_execution_date = input()

            print("Pls, enter name of user, for whom this job was created")
            jobs_executor = input()

            return jobs_name, jobs_condition, jobs_group, jobs_priority, jobs_execution_date, jobs_executor

        elif jobs_type == "reminder":

            print("Pls, enter name of the job"
                  "(short name, it'll used for interactions)")
            jobs_name = input()

            print("Pls, enter condition of the job"
                  "(detailed explanation)")
            jobs_condition = input()

            print("Pls, enter frequency of the job"
                  "('ED' ~ every day, 'EM' ~ every month, 'EY' ~ every year)")
            job_frequency = input()

            print("Pls, enter date, when this job may be started"
                  "(day, month, year)")
            jobs_start_date = input()

            print("Pls, enter date, when this job may be finished"
                  "(day, month, year)")
            jobs_finish_date = input()

            print("Pls, enter name of user, for whom this job was created")
            jobs_executor = input()

            return jobs_name, jobs_condition, job_frequency, jobs_start_date, jobs_finish_date, jobs_executor

    # stage four: to say user, that all is ok
    if stage == 4:

        print("All done")

    # stage five(only in case of error): if something was wrong, need to print it for user
    if stage == 5:

        print("Something was wrong, pls try again")


def incremental_searching(stage, searching_job):

    # communicate with user for printing and inputting information or give the other functions users choice
    # while user try to find something

    # stage is a variable, which explains what stage of the long process is now required to provide the user

    # stage one is choosing what user want to find: task or reminder
    if stage == 1:

        print("Pls, enter what type of job do you want to find"
              "('t' ~ task, 'r' ~ reminder)")
        users_choice = input()

        return users_choice

    # stage two is inputting name of the job, that user want to find
    if stage == 2:

        print("Pls, enter name of job, that you want to find")
        users_name = input()

        return users_name

    # stage three: print all information about the job, that user found
    if stage == 3:

        # if there is a task print task parameters
        if searching_job.priority:
            print("Found task:")
            print("Name: ", searching_job.name)
            print("Condition: ", searching_job.condition)
            print("Priority: ", searching_job.priority)
            print("Date of execution: ", searching_job.date_of_execution)
            print("Executor: ", searching_job.executor)
            if searching_job.list_of_sub_tasks:
                print("List_of_sub_tasks: ", searching_job.list_of_sub_tasks)
            if searching_job.written_by:
                print("Written_by: ", searching_job.written_by)

        # else print information about reminder
        else:
            print("Found reminder:")
            print("Name: ", searching_job.name)
            print("Condition: ", searching_job.condition)
            print("Frequency: ", searching_job.frequency)
            print("Start date: ", searching_job.start_date)
            print("Finish date: ", searching_job.finish_date)
            print("Executor: ", searching_job.executor)


    # stage five(only in case of error): if something was wrong, need to print it for user
    if stage == 5:

        print("Something was wrong, pls try again")


def incremental_editing(stage, old_job):

    # communicate with user for printing and inputting information or give the other functions users choice
    # while user try to edit something

    # stage is a variable, which explains what stage of the long process is now required to provide the user

    # stage one is choosing what user want to edit: task or reminder
    if stage == 1:

        print("Pls, enter what type of job do you want to edit"
              "('t' ~ task, 'r' ~ reminder)")
        users_choice = input()

        return users_choice

    # stage two is inputting name of the job, that user want to edit
    if stage == 2:

        print("Pls, enter name of job, that you want to edit")
        users_name = input()

        return users_name

    # stage three is inputting info about the job
    if stage == 3:

        # if there is a task-parameter
        if old_job.priority:

            # there is not combo_task or redirected_task parameter
            # so, need to update simple_task
            if not old_job.list_of_sub_tasks:

                print("Pls, enter new condition of the task")
                new_condition_of_job = input()

                print("Pls, enter new group of the task"
                      "('Personal', 'Work', etc)")
                new_group_of_job = input()

                print("Pls, enter new priority of the task"
                      "('1' ~ high, '2' ~ medium, '3' ~ low)")
                new_priority_of_job = input()

                print("Pls, enter new date, when this task may be finished"
                      "(day, month, year)")
                new_date_of_execution_of_job = input()

                # creating new task-object
                updated_job = task.SimpleTask(old_job.name, new_condition_of_job, new_group_of_job, new_priority_of_job,
                                              new_date_of_execution_of_job, old_job.executor, old_job.status)

                return updated_job

            # there is not redirected_task parameter
            # so, need to update combo_task
            elif not old_job.written_by:

                print("Pls, enter new condition of the task")
                new_condition_of_job = input()

                print("Pls, enter new group of the task"
                      "('Personal', 'Work', etc)")
                new_group_of_job = input()

                print("Pls, enter new priority of the task"
                      "('1' ~ high, '2' ~ medium, '3' ~ low)")
                new_priority_of_job = input()

                print("Pls, enter new date, when this task may be finished"
                      "(day, month, year)")
                new_date_of_execution_of_job = input()

                # creating new task-object
                updated_job = task.ComboTask(old_job.name, new_condition_of_job, new_group_of_job, new_priority_of_job,
                                             new_date_of_execution_of_job, old_job.executor, old_job.status,
                                             old_job.list_of_sub_tasks)

                return updated_job

            # there is only redirected_task parameter
            # so, need to update redirected_task
            elif old_job.written_by:

                print("Pls, enter new condition of the task")
                new_condition_of_job = input()

                print("Pls, enter new group of the task"
                      "('Personal', 'Work', etc)")
                new_group_of_job = input()

                print("Pls, enter new priority of the task"
                      "('1' ~ high, '2' ~ medium, '3' ~ low)")
                new_priority_of_job = input()

                print("Pls, enter new date, when this task may be finished"
                      "(day, month, year)")
                new_date_of_execution_of_job = input()

                # creating new task-object
                updated_job = task.RedirectedTask(old_job.name, new_condition_of_job, new_group_of_job,
                                                  new_priority_of_job, new_date_of_execution_of_job,
                                                  old_job.written_by, old_job.executor, old_job.status,
                                                  old_job.list_of_sub_tasks)

                return updated_job

        # else, need to update reminder
        else:

            print("Pls, enter new condition of the reminder")
            new_condition_of_job = input()

            print("Pls, enter new frequency of the reminder"
                  "('ED' ~ every day, 'EM' ~ every month, 'EY' ~ every year)")
            new_frequency_of_job = input()

            print("Pls, enter new date, when this reminder may be finished"
                  "(day, month, year)")
            new_finish_date_of_job = input()

            # creating new reminder-object
            updated_job = reminder.Reminder(old_job.name, new_condition_of_job, new_frequency_of_job, old_job.start_date,
                                            new_finish_date_of_job, old_job.executor)

            return updated_job

    # stage four: to say user, that all is ok
    if stage == 4:

        print("All done")

    # stage five(only in case of error): if something was wrong, need to print it for user
    if stage == 5:

        print("Something was wrong, pls try again")


def incremental_checking(stage, tasks, reminders):

    # communicate with user for printing and inputting information or give the other functions users choice
    # while user try to check jobs for current user

    # stage is a variable, which explains what stage of the long process is now required to provide the user

    # first stage is choosing what user want to do? (check all jobs or only its jobs for this day)
    if stage == 1:

        print("Pls, enter what do you want to see"
              "('all' ~ ALL your jobs, 'this' ~ jobs fir THIS day)")
        users_choice = input()

        return users_choice

    # last stage: print all information about the jobs for that user for this day
    if stage == 2:

        # print tasks
        for check_task in tasks:

            # print every task in tasks-list
            print("Task:")
            print("Name: ", check_task.name)
            print("Condition: ", check_task.condition)
            print("Group: ", check_task.group)
            print("Priority: ", check_task.priority)
            print("Date of execution: ", check_task.date_of_execution)
            print("Executor: ", check_task.executor)
            if check_task.list_of_sub_tasks:
                print("List_of_sub_tasks: ", check_task.list_of_sub_tasks)
            if check_task.written_by:
                print("Written_by: ", check_task.written_by)
            print("")

        # print reminders
        for check_reminder in reminders:

            # print every reminder in reminder-list
            print("Reminder:")
            print("Name: ", check_reminder.name)
            print("Condition: ", check_reminder.condition)
            print("Frequency: ", check_reminder.frequency)
            print("Start date: ", check_reminder.start_date)
            print("Finish date: ", check_reminder.finish_date)
            print("Executor: ", check_reminder.executor)
            print("")


def incremental_deleting(stage):

    # communicate with user for printing and inputting information or give the other functions users choice
    # while user try to delete something

    # stage is a variable, which explains what stage of the long process is now required to provide the user

    # stage one is choosing what user want to delete: task or reminder
    if stage == 1:

        print("Pls, enter what type of job do you want to create"
              "('t' ~ task, 'r' ~ reminder)")
        users_choice = input()

        return users_choice

    # stage two is inputting name of the job, that user want to delete
    if stage == 2:

        print("Pls, enter name of job, that you want to delete")
        users_name = input()

        return users_name

    # stage three: to say user, that all is done
    if stage == 3:

        print("All done")


def incremental_authorisation(stage):

    # communicate with user for printing and inputting information or give the other functions users choice
    # while user try to do something with user_names and its passwords or to change active user

    # stage is a variable, which explains what stage of the long process is now required to provide the user

    # stage one is choosing what username user want to do
    if stage == 1:

        print("Pls, enter what do you want to do"
              "('n' ~ create new user, 'c' ~ change current user)")
        users_choice = input()

        return users_choice

    # stage two is inputting name of the user
    if stage == 2:

        print("Pls, enter name of user, that you want to use")
        users_name = input()

        return users_name

    # stage three is inputting password
    if stage == 3:

        print("Pls, enter password of user, that you want to use")
        users_password = input()

        return users_password

    # stage four: to say user, that all is ok
    if stage == 4:

        print("All done")


def help_output():

    # it just print info about how to use program

    print("For creating new job write argument '-n'")
    print("For searching job write argument '-f'")
    print("For editing job write argument '-e'")
    print("For deleting job write argument '-d'")
    print("For work with users write argument '-a'")
    print("For checking jobs for user write argument '-c'")
