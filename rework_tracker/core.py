#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from rework_tracker.reminder_manager import reminder_manager, reminder
from rework_tracker.task_manager import task_manager, task
from rework_tracker.user_manager import user_manager, user
from rework_tracker.storage import storage
from rework_tracker.console_manager import console_users_interactions
from rework_tracker.console_manager import console_user_manager
from rework_tracker import exceptions
from rework_tracker import inspector
from rework_tracker import config
from rework_tracker import logger
import argparse


def authorisation(users_manager):

    # all interactions with user are carried out using special functions
    # stage one: what user want to do (create new user, edit existing user or change active user)?
    users_choice = console_users_interactions.incremental_authorisation(1)

    if users_choice == "n":

        # stage two is inputting name and password, that user want to use
        users_name = console_users_interactions.incremental_authorisation(2)
        users_password = console_users_interactions.incremental_authorisation(3)

        try:

            # create new User object
            new_user = user.User(users_name, users_password, "N")

            # give object to user_manager for creating new user
            users_manager.new_user(new_user)

            # stage three: to say user, that all is ok
            console_users_interactions.incremental_authorisation(4)

            # save it into logs
            logger.end_logger()

        except:
            exceptions.Error()

            # save it into logs
            logger.error_logger()

    elif users_choice == "c":

        # stage two is inputting name, that user want to use
        users_name = console_users_interactions.incremental_authorisation(2)

        try:

            # stage three is inputting password for authentication
            users_password = console_users_interactions.incremental_authorisation(3)
            verdict = inspector.password_check(users_name, users_password)

            if verdict:
                users_manager.change_current_user(user)

                # stage four: to say user, that all is ok
                console_users_interactions.incremental_authorisation(4)

                # save it into logs
                logger.end_logger()

        except:
            exceptions.Error()

            # save it into logs
            logger.error_logger()

    else:
        exceptions.Error()

        # save it into logs
        logger.error_logger()


def checking_jobs(tasks_manager, reminders_manager, current_users_name):

    # all interactions with user are carried out using special functions
    # first stage: what user want to do?
    users_choice = console_users_interactions.incremental_checking(1, None, None)

    if users_choice == "all":

        # all jobs of current user
        tasks_for_current_user = tasks_manager.check_tasks_all(current_users_name)
        reminders_for_current_user = reminders_manager.check_reminders_all(current_users_name)

        # last stage: print all information
        console_users_interactions.incremental_checking(2, tasks_for_current_user, reminders_for_current_user)

        # save it into logs
        logger.end_logger()

    elif users_choice == "this":

        # jobs for current user for this day
        tasks_for_current_user = tasks_manager.check_tasks_for_this_day(current_users_name)
        reminders_for_current_user = reminders_manager.check_reminders_for_this_day(current_users_name)

        # last stage: print all information
        console_users_interactions.incremental_checking(2, tasks_for_current_user, reminders_for_current_user)

        # save it into logs
        logger.end_logger()

    else:
        exceptions.Error()

        # save it into logs
        logger.error_logger()


def deleting_jobs(tasks_manager, reminders_manager, current_users_name):

    # all interactions with user are carried out using special functions
    # stage one: what user want to delete (task or reminder)?
    users_choice = console_users_interactions.incremental_deleting(1)

    if users_choice == "t":

        # stage two is inputting name of the job, that user want to delete
        tasks_name = console_users_interactions.incremental_deleting(2)

        try:
            tasks_manager.delete_task(current_users_name, tasks_name)

            # stage three: to say user, that all is ok
            console_users_interactions.incremental_deleting(3)

            # save it into logs
            logger.end_logger()

        except:
            exceptions.Error()

            # save it into logs
            logger.error_logger()

    elif users_choice == "r":

        # stage two is inputting name of the job, that user want to delete
        reminders_name = console_users_interactions.incremental_deleting(2)

        try:
            reminders_manager.delete_reminder(current_users_name, reminders_name)

            # stage three: to say user, that all is ok
            console_users_interactions.incremental_deleting(3)

            # save it into logs
            logger.end_logger()

        except:
            exceptions.Error()

            # save it into logs
            logger.error_logger()

    else:
        exceptions.Error()


def editing_job(tasks_manager, reminders_manager, current_users_name):

    # all interactions with user are carried out using special functions
    # stage one: what user want to edit (task or reminder)?
    users_choice = console_users_interactions.incremental_editing(1, None)

    if users_choice == "t":

        # stage two is inputting name of the job, that user want to edit
        tasks_name = console_users_interactions.incremental_editing(2, None)

        try:
            searching_job = tasks_manager.load_task(current_users_name, tasks_name)

            # stage three is inputting new info about the job
            update_job = console_users_interactions.incremental_editing(3, searching_job)

            tasks_manager.edit_task(current_users_name, searching_job, update_job)

            # stage four: to say user, that all is ok
            console_users_interactions.incremental_editing(4, None)

            # save it into logs
            logger.end_logger()

        except:

            # stage five(only in case of error): if something was wrong, need to print it for user
            exceptions.Error()
            console_users_interactions.incremental_editing(5, None)

            # save it into logs
            logger.error_logger()

    elif users_choice == "r":

        # stage two is inputting name of the job, that user want to edit
        reminders_name = console_users_interactions.incremental_editing(2, None)

        try:
            searching_job = reminders_manager.load_reminder(current_users_name, reminders_name)

            # stage three is inputting new info about the job
            update_job = console_users_interactions.incremental_editing(3, searching_job)

            reminders_manager.edit_reminder(current_users_name, searching_job, update_job)

            # stage four: to say user, that all is ok
            console_users_interactions.incremental_editing(4, None)

            # save it into logs
            logger.end_logger()

        except:

            # stage five(only in case of error): if something was wrong, need to print it for user
            exceptions.Error()
            console_users_interactions.incremental_editing(5, None)

            # save it into logs
            logger.error_logger()

    else:
        exceptions.Error()

        # save it into logs
        logger.error_logger()


def finding_job(tasks_manager, reminders_manager, current_users_name):

    # all interactions with user are carried out using special functions
    # stage one: what user want to find (task or reminder)?
    users_choice = console_users_interactions.incremental_searching(1, None)

    if users_choice == "t":

        # stage two is inputting name of the job, that user want to find
        tasks_name = console_users_interactions.incremental_searching(2, None)

        try:
            searching_job = tasks_manager.load_task(current_users_name, tasks_name)

            # stage three: print all information about the job, that user found
            console_users_interactions.incremental_searching(3, searching_job)

            # save it into logs
            logger.end_logger()

        except:

            # stage five(only in case of error): if something was wrong, need to print it for user
            exceptions.Error()
            console_users_interactions.incremental_searching(5, None)

            # save it into logs
            logger.error_logger()

    elif users_choice == "r":

        # stage two is inputting name of the job, that user want to find
        reminders_name = console_users_interactions.incremental_searching(2, None)

        try:
            searching_job = reminders_manager.load_reminder(current_users_name, reminders_name)

            # stage three: print all information about the job, that user found
            console_users_interactions.incremental_searching(3, searching_job)

            # save it into logs
            logger.end_logger()

        except:

            # stage five(only in case of error): if something was wrong, need to print it for user
            exceptions.Error()
            console_users_interactions.incremental_searching(5, None)

            # save it into logs
            logger.error_logger()

    else:
        exceptions.Error()


def creating_job(tasks_manager, reminders_manager, current_users_name):

    # all interactions with user are carried out using special functions
    # stage one: what user want to create (task or reminder)?
    users_choice = console_users_interactions.incremental_creating(1, None)

    if users_choice == "t":

        # stage two: what exactly task user want to create (simple_task, combo_task or redirected_task)?
        users_choice = console_users_interactions.incremental_creating(2, None)

        if users_choice == "s":

            # stage three: input parameters of new simple task
            new_tasks_name, new_tasks_condition, new_tasks_group, new_tasks_priority, new_tasks_date_of_execution = \
                console_users_interactions.incremental_creating(3, "simple_task")

            # creating object
            new_simple_task = task.SimpleTask(new_tasks_name, new_tasks_condition, new_tasks_group,
                                              new_tasks_priority, new_tasks_date_of_execution,
                                              current_users_name, "In_progress")

            try:

                # transmission SimpleTask object to task_manager for creating new simple task
                tasks_manager.new_simple_task(new_simple_task)

                # stage four: to say user, that all is ok
                console_users_interactions.incremental_creating(4, None)

                # save it into logs
                logger.end_logger()

            except:

                # stage five(only in case of error): if something was wrong, need to print it for user
                exceptions.Error()
                console_users_interactions.incremental_creating(5, None)

                # save it into logs
                logger.error_logger()

        elif users_choice == "c":

            # stage three: input parameters of new combo task
            new_tasks_name, new_tasks_condition, new_tasks_group, new_tasks_priority, new_tasks_date_of_execution = \
                console_users_interactions.incremental_creating(3, "combo_task")

            # creating object
            new_tasks_list_of_sub_tasks = list()
            new_combo_task = task.ComboTask(new_tasks_name, new_tasks_condition, new_tasks_group,
                                            new_tasks_priority, new_tasks_date_of_execution,
                                            current_users_name, "In_progress", new_tasks_list_of_sub_tasks)

            try:

                # transmission ComboTask object to task_manager for creating new combo task
                tasks_manager.new_combo_task(new_combo_task)

                # stage four: to say user, that all is ok
                console_users_interactions.incremental_creating(4, None)

                # save it into logs
                logger.end_logger()

            except:

                # stage five(only in case of error): if something was wrong, need to print it for user
                exceptions.Error()
                console_users_interactions.incremental_creating(5, None)

                # save it into logs
                logger.error_logger()

        elif users_choice == "r":

            # stage three: input parameters of new redirected task
            new_tasks_name, new_tasks_condition, new_tasks_group, new_tasks_priority, new_tasks_date_of_execution, \
            new_tasks_executor = console_users_interactions.incremental_creating(3, "redirected_task")

            # creating object
            new_tasks_list_of_sub_tasks = list()
            new_redirected_task = task.RedirectedTask(new_tasks_name, new_tasks_condition, new_tasks_group,
                                                      new_tasks_priority, new_tasks_date_of_execution,
                                                      new_tasks_executor, current_users_name,
                                                      "In_progress", new_tasks_list_of_sub_tasks)

            try:

                # transmission RedirectedTask object to task_manager for creating new redirected task
                tasks_manager.new_redirected_task(new_redirected_task)

                # stage four: to say user, that all is ok
                console_users_interactions.incremental_creating(4, None)

                # save it into logs
                logger.end_logger()

            except:

                # stage five(only in case of error): if something was wrong, need to print it for user
                exceptions.Error()
                console_users_interactions.incremental_creating(5, None)

                # save it into logs
                logger.error_logger()

        else:
            exceptions.Error()

            # save it into logs
            logger.error_logger()

    elif users_choice == "r":

        # stage three: input parameters of new redirected task
        new_reminder_name, new_reminder_condition, new_reminder_frequency, new_reminder_start_date, \
        new_reminder_finish_date, new_reminder_executor = \
            console_users_interactions.incremental_creating(3, "reminder")

        # creating object
        new_reminder = reminder.Reminder(new_reminder_name, new_reminder_condition, new_reminder_frequency,
                                         new_reminder_start_date, new_reminder_finish_date,
                                         new_reminder_executor)

        try:

            # transmission Reminder object to reminder_manager for creating new reminder
            reminders_manager.new_reminder(new_reminder)

            # stage four: to say user, that all is ok
            console_users_interactions.incremental_creating(4, None)

            # save it into logs
            logger.end_logger()

        except:

            # stage five(only in case of error): if something was wrong, need to print it for user
            exceptions.Error()
            console_users_interactions.incremental_creating(5, None)

            # save it into logs
            logger.error_logger()

    else:
        exceptions.Error()

        # save it into logs
        logger.error_logger()


def arguments_parser():

    # parser
    parser = argparse.ArgumentParser(add_help=False)

    parser.add_argument('-n', '--new', action='store_true')
    parser.add_argument('-h', '--help', action='store_true')
    parser.add_argument('-f', '--find', action='store_true')
    parser.add_argument('-e', '--edit', action='store_true')
    parser.add_argument('-c', '--check', action='store_true')
    parser.add_argument('-t', '--tests', action='store_true')
    parser.add_argument('-d', '--delete', action='store_true')
    parser.add_argument('-a', '--authorization', action='store_true')

    parser.add_argument('-dry', '--dry', action='store_true')
    parser.add_argument('-silent', '--silent', action='store_true')

    args = parser.parse_args()

    parser_agr = ""
    parser_modification = ""

    # processing of input primary arguments
    if args.new:
        parser_agr = "NEW"

    elif args.help:
        parser_agr = "HELP"

    elif args.find:
        parser_agr = "FIND"

    elif args.edit:
        parser_agr = "EDIT"

    elif args.check:
        parser_agr = "CHECK"

    elif args.tests:
        parser_agr = "TEST"

    elif args.delete:
        parser_agr = "DELETE"

    elif args.authorization:
        parser_agr = "AUTH"

    # processing of input secondary arguments
    if args.dry:
        parser_modification = "DRY"

    elif args.silent:
        parser_modification = "SILENT"

    return parser_agr, parser_modification


def main():

    # include logger
    logger.create_logger()

    # receiving user-entered parameters
    primary_input_arg, secondary_input_arg = arguments_parser()

    # check and load paths
    configuration = config.load_config()
    info_storage = storage.Storage(configuration.get('Paths', 'tasks_path'),
                                   configuration.get('Paths', 'reminders_path'),
                                   configuration.get('Paths', 'users_path'))

    tasks_manager = task_manager.TaskManager(info_storage)
    reminders_manager = reminder_manager.ReminderManager(info_storage)
    users_manager = user_manager.UserManager(info_storage)

    # check for at least one user
    is_one_user = inspector.user_check()

    if is_one_user:
        current_users_name = console_user_manager.check_current_user()
    else:

        # all interactions with user are carried out using special functions
        # stage one is choosing what username and password user want to use
        new_users_name = console_users_interactions.incremental_authorisation(2)
        new_users_password = console_users_interactions.incremental_authorisation(3)

        # creating new User object
        new_current_user = user.User(new_users_name, new_users_password, "Y")

        # transmission object to user_manager for creating new user
        users_manager.new_user(new_current_user)

        current_users_name = new_current_user.name

    # processing of the values of the entered parameters

    # primary argument "h" ~ help info for user
    if primary_input_arg == "HELP":

        # print all information about "how to use it" with all primary and/or secondary args
        console_users_interactions.help_output()

    # primary argument "n" ~ new(task or reminder)
    if primary_input_arg == "NEW":

        creating_job(tasks_manager, reminders_manager, current_users_name)

    # primary argument "f" ~ searching(task or reminder)
    if primary_input_arg == "FIND":

        finding_job(tasks_manager, reminders_manager, current_users_name)

    # primary argument "e" ~ edit(task or reminder)
    if primary_input_arg == "EDIT":

        editing_job(tasks_manager, reminders_manager, current_users_name)

    # primary argument "c" ~ check job for this day for current user or all jobs for user
    if primary_input_arg == "CHECK":

        checking_jobs(tasks_manager, reminders_manager, current_users_name)

    #  primary argument "t" ~ running unittests
    #  if primary_input_arg == "t":
    #      tests()

    # primary argument "d" ~ delete(task or reminder)
    if primary_input_arg == "DELETE":

        deleting_jobs(tasks_manager, reminders_manager, current_users_name)


    # primary argument "a" ~ operation with users(new, edit, change_current_user)
    if primary_input_arg == "AUTH":

        authorisation(users_manager)

    # primary argument "" ~ nothing was chosen
    if primary_input_arg == "":
        exceptions.Error()

        # save it into logs
        logger.error_logger()


if __name__ == "__main__":
    main()
