#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from rework_tracker import exceptions


class UserManager:
    def __init__(self, storage):
        self.storage = storage

    def new_user(self, user):

        # check user with such name
        user_info = self.storage.load_user_by_name(user.name, user.password)

        #create new user
        if not user_info:
            content = {'name': user.name,
                       'password': user.password,
                       'current' : user.current,
                       }

            # and save it
            self.storage.add_user(content)
        else:
            raise exceptions.Error()
