#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


class SimpleTask:

    def __init__(self, name, condition, group, priority, date_of_execution, executor, status):
        self.name = name # 'name' is a unique identifier of a task
        self.condition = None # 'condition' is a detail description of task
        self.group = None # 'group' is a name for grouping task groups
        self.priority = None # '1' - high, '2' - medium, '3' - low priority for execute
        self.date_of_execution = None  # 'date_of_execution' is a date, when a task must be execute
        self.executor = executor # 'executor' is a user, who must execute this job
        self.status = "In_progress" # 'status' is a state of a task('In_progress', 'Failed', etc.)


class ComboTask:

    def __init__(self, name, condition, group, priority, date_of_execution, executor, status, list_of_sub_tasks):
        self.name = name # 'name' is a unique identifier of a task
        self.condition = None # 'condition' is a detail description of task
        self.group = None # 'group' is a name for grouping task groups
        self.priority = None # '1' - high, '2' - medium, '3' - low priority for execute
        self.date_of_execution = None  # 'date_of_execution' is a date, when a task must be execute
        self.executor = executor # 'executor' is a user, who must execute this job
        self.status = "In_progress" # 'status' is a state of a task('In_progress', 'Failed', etc.)
        self.list_of_sub_tasks = None # 'list_of_sub_tasks' is a list of sub_tasks of job


class RedirectedTask:

    def __init__(self, name, condition, group, priority, date_of_execution, executor, written_by,
                 status, list_of_sub_tasks):
        self.name = name # 'name' is a unique identifier of a task
        self.condition = None # 'condition' is a detail description of task
        self.group = None # 'group' is a name for grouping task groups
        self.priority = None # '1' - high, '2' - medium, '3' - low priority for execute
        self.date_of_execution = None  # 'date_of_execution' is a date, when a task must be execute
        self.executor = executor # 'executor' is a user, who must execute this job
        self.written_by = written_by # 'written_by' is a user, who create this task
        self.status = "In_progress" # 'status' is a state of the task('In_progress', 'Failed', etc.)
        self.list_of_sub_tasks = None # 'list_of_sub_tasks' is a list of sub_tasks of job
