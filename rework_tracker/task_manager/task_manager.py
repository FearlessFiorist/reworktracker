#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from rework_tracker import exceptions
from rework_tracker import inspector
import datetime


class TaskManager:
    def __init__(self, storage):
        self.storage = storage

    def new_simple_task(self, simple_task):
        #date_of_execution = inspector.date_check(simple_task.date_of_execution)
        new_task = {'name': simple_task.name,
                    'condition': simple_task.condition,
                    'group': simple_task.group,
                    'priority': simple_task.priority,
                    #'date_of_execution': date_of_execution.strftime('%d.%m.%Y %H:%M'),
                    'date_of_execution': simple_task.date_of_execution,
                    'executor': simple_task.executor,
                    'status': simple_task.status
                    }
        self.storage.add_task(new_task)

    def new_combo_task(self, combo_task):
        #date_of_execution = inspector.date_check(combo_task.date_of_execution)
        new_task = {'name': combo_task.name,
                    'condition': combo_task.condition,
                    'group': combo_task.group,
                    'priority': combo_task.priority,
                    #'date_of_execution': date_of_execution.strftime('%d.%m.%Y %H:%M'),
                    'date_of_execution': combo_task.date_of_execution,
                    'executor': combo_task.executor,
                    'status': combo_task.status,
                    'list_of_subs_tasks': combo_task.list_of_sub_tasks
                    }
        self.storage.add_task(new_task)

    def new_redirected_task(self, redirected_task):
        #date_of_execution = inspector.date_check(redirected_task.date_of_execution)
        new_task = {'name': redirected_task.name,
                    'condition': redirected_task.condition,
                    'group': redirected_task.group,
                    'priority': redirected_task.priority,
                    #'date_of_execution': date_of_execution.strftime('%d.%m.%Y %H:%M'),
                    'date_of_execution': redirected_task.date_of_execution,
                    'executor': redirected_task.executor,
                    'written_by': redirected_task.written_by,
                    'status': redirected_task.status,
                    'list_of_sub_tasks': redirected_task.list_of_sub_tasks
                    }
        self.storage.add_task(new_task)

    def load_task(self, users_name, tasks_name):

        # searching task by its name

        # if the task exists for this user - we'll get it
        task = self.storage.load_task_by_name(users_name, tasks_name)

        # and return it
        return task

    def edit_task(self, users_name, old_task, updated_task):

        # edit already exists task

        # if the task exists for this user - we'll get it
        task = self.storage.load_task_by_name(users_name, old_task.name)

        # if some parameters of task was changed - we'll update it
        if task:
            if old_task.condition != updated_task.condition:
                task.update({'content': updated_task.condition})
            if old_task.priority != updated_task.priority:
                task.update({'priority': updated_task.priority})
            if old_task.group != updated_task.group:
                task.update({'group': updated_task.group})
            if old_task.date_of_execution != updated_task.date_of_execution:
                task.update({'execution_date': updated_task.date_of_execution.strftime('%d.%m.%Y %H:%M')})

            # send information for rewrite
            self.storage.edit_task(task)
        else:
            raise exceptions.Error()

    def delete_task(self, users_name, tasks_name):

        # deleting task by its name

        # if the task exists for this user - delete it
        task = self.storage.load_task_by_name(users_name, tasks_name)
        if task:
            self.storage.delete_task(task)
        else:
            raise exceptions.Error()

        # if this task had sub_tasks - delete all sub_tasks too
        if task.list_of_sub_tasks:
            for i in range(len(task.list_of_sub_tasks)):
                sub_task_name = task.list_of_sub_tasks[i]
                self.storage.delete_task(sub_task_name)

    def add_sub_task(self, main_task, sub_tasks_name):

        # adding sub_task to combo_ or redirected_task

        # load all tasks
        tasks = self.storage.load_all_tasks()

        # searching for a necessary task and add sub_task_name to the list_of_subs
        for i, task in enumerate(tasks):
            if task.get('name') == main_task.name:
                new_list_of_sub_tasks = main_task.list_of_sub_tasks.append(sub_tasks_name)
                tasks[i].update({'list_of_sub_tasks': new_list_of_sub_tasks})

        # save updated list of tasks
        self.storage.save_tasks(tasks)


    def check_tasks_all(self, user_name):

        # check all tasks for current user
        list_of_tasks_for_return = list()

        # load all tasks
        tasks = self.storage.load_all_tasks()

        # searching for a necessary tasks and add it to list of return tasks
        for i, task in enumerate(tasks):
            if task.get('executor') == user_name:
                list_of_tasks_for_return.append(task.name)

        return list_of_tasks_for_return

    def check_tasks_for_this_day(self, user_name):

        # check tasks for current user only for this day
        list_of_tasks_for_return = list()
        today = datetime.datetime.now()

        # load all tasks
        tasks = self.storage.load_all_tasks()

        # searching for a necessary tasks and add it to list of return tasks
        for i, task in enumerate(tasks):
            if task.get('executor') == user_name:
                if task.get('date_of_execution') <= today:
                    list_of_tasks_for_return.append(task.name)

        return list_of_tasks_for_return


    def load_all_dates_of_execution(self, user_name):

        # check all possible dates of tasks
        list_of_dates_for_return = list()

        # load all tasks
        tasks = self.storage.load_all_tasks()

        # searching for a necessary tasks and add it to list of return tasks
        for i, task in enumerate(tasks):
            if task.get('executor') == user_name:
                list_of_dates_for_return.append(task.date_of_execution.strftime('%d.%m.%Y'))

        return list_of_dates_for_return


    def edit_status_of_tasks(self):

        # edit status "In_progress" to "Failed" for all tasks if date of execution is overdue

        # load all tasks
        tasks = self.storage.load_all_tasks()

        # now is a variable, that contains info about today
        today = datetime.datetime.today()
        now = today.strftime('%d.%m.%Y')

        # searching for a necessary tasks and add it to list of return tasks
        for i, task in enumerate(tasks):
            if task.get('date_of_execution') < now:
                task.update({'status': "Failed  "})

                self.storage.edit_task(task)
