#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


class Reminder:

    def __init__(self, name, condition, frequency, start_date, finish_date, executor):
        self.name = name # 'name' is a unique identifier of a reminder
        self.condition = None # 'condition' is a detail description of task
        self.frequency = None # 'frequency' is how often program will talk user about this job
        self.start_date = None # it's a date, when reminder will start
        self.finish_date = None # it's a date, when reminder will deactivate
        self.executor = executor # 'executor' is a user, who must execute this job
