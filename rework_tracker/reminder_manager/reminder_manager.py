#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from rework_tracker import exceptions
import datetime


class ReminderManager:
    def __init__(self, storage):
        self.storage = storage

    def new_reminder(self, reminder):

        # check reminders with such name
        reminder_info = self.storage.load_reminder_by_name(reminder.executor, reminder.name)

        #create new reminder
        if not reminder_info:
            new_reminder = {'name': reminder.name,
                            'condition': reminder.condition,
                            'priority': reminder.frequency,
                            'start_date': reminder.start_date.strftime('%d.%m.%Y %H:%M'),
                            'finish_date': reminder.finish_date.strftime('%d.%m.%Y %H:%M'),
                            'executor': reminder.executor,
                            }

            # and save it
            self.storage.add_reminder(new_reminder)
        else:
            raise exceptions.Error()

    def load_reminder(self, users_name, reminders_name):

        # if the reminder exists for this user - we'll get it
        reminder = self.storage.load_reminder_by_name(users_name, reminders_name)

        # and return it
        return reminder

    def edit_reminder(self, users_name, old_reminder, updated_reminder):

        # if the reminder exists for this user - we'll get it
        reminder = self.storage.load_reminder_by_name(users_name, old_reminder.name)

        # if some parameters of reminder was changed - we'll update it
        if reminder:
            if old_reminder.condition != updated_reminder.condition:
                reminder.update({'content': updated_reminder.condition})
            if old_reminder.frequency != updated_reminder.frequency:
                reminder.update({'frequency': updated_reminder.frequency})
            if old_reminder.finish_date != updated_reminder.finish_date:
                reminder.update({'finish_date': updated_reminder.finish_date.strftime('%d.%m.%Y %H:%M')})

            # send information for rewrite
            self.storage.edit_reminder(reminder)
        else:
            raise exceptions.Error()

    def delete_reminder(self, users_name, reminders_name):

        # if the reminder exists for this user - delete it
        reminder = self.storage.load_reminder_by_name(users_name, reminders_name)
        if reminder:
            self.storage.delete_reminder(reminder)
        else:
            raise exceptions.Error()

    def check_reminders_all(self, user_name):
        # check reminders for current user

        list_of_reminders_for_return = list()

        # load all reminders
        reminders = self.storage.load_all_reminders()

        # searching for a necessary reminders and add it to list of return reminders
        for i, reminder in enumerate(reminders):
            if reminder.get('executor') == user_name:
                list_of_reminders_for_return.append(reminder.name)

        return list_of_reminders_for_return

    def check_reminders_for_this_day(self, user_name):
        # check reminders for current user only for this day

        list_of_reminders_for_return = list()
        today = datetime.datetime.now()

        # load all reminders
        reminders = self.storage.load_all_reminders()

        # searching for a necessary reminders and add it to list of return reminders
        for i, reminder in enumerate(reminders):
            if reminder.get('executor') == user_name:
                if reminder.get('finish_date') <= today:
                    list_of_reminders_for_return.append(reminder.name)

        return list_of_reminders_for_return
