#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import logging
import os


def create_logger():

    #  logger description
    logger = logging.getLogger("MainLogger")
    logger.setLevel(logging.INFO)

    #  create the logging file handler
    default_logs_path = "logs.log"
    if not os.path.exists(default_logs_path):
        file = open(default_logs_path, 'w')
    fh = logging.FileHandler(default_logs_path)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)

    #  add handler to logger object
    logger.addHandler(fh)


def end_logger():

    #  logger description
    logger = logging.getLogger("MainLogger")
    logger.setLevel(logging.INFO)

    #  create the logging file handler
    default_logs_path = "logs.log"
    fh = logging.FileHandler(default_logs_path)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)

    #  add handler to logger object
    logger.addHandler(fh)

    logger.info("")


def error_logger():

    #  logger description
    logger = logging.getLogger("MainLogger")
    logger.setLevel(logging.INFO)

    #  create the logging file handler
    default_logs_path = "logs.log"
    fh = logging.FileHandler(default_logs_path)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)

    #  add handler to logger object
    logger.addHandler(fh)

    logger.error("")