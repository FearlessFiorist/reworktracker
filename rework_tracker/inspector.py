#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from datetime import datetime
import os


def user_check():
    # check for at least one user

    # ...

    return 0


def path_check(path):
    # check for a file exists on a given path

    if os.path.exists(path):
        return True
    else:
        return False


def date_check(value):
    # check for valid date

    current_date = datetime.strptime(datetime.now().strftime('%d.%m.%Y %H:%M'), '%d.%m.%Y %H:%M')
    execution_date = datetime.strptime(value, '%d.%m.%Y %H:%M')
    if (current_date - execution_date).total_seconds() >= 0:
        return None
    else:
        return execution_date


def password_check(user_name, password):
    # check inputted password by current user and password of user in base

    verdict = True  # or False

    # ...

    return verdict