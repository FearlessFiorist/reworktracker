#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

from rework_tracker import inspector
from rework_tracker import exceptions
import json
import os


class Storage:
    def __init__(self, task_path_in, reminder_path_in, user_path_in):
        self.task_path = self.path_set(task_path_in)
        self.reminder_path = self.path_set(reminder_path_in)
        self.user_path = self.path_set(user_path_in)

    @staticmethod
    def path_set(value):

        # check of valid date
        if inspector.path_check(value):
            return value
        else:
            return exceptions.Error


    def load_task_by_name(self, user_name, task_name):

        # load all tasks from json-file by name with check of current user
        with open(os.path.join(self.task_path, 'tasks.json'), 'r') as outfile:
            tasks = json.load(outfile)

        for task in tasks:
            if task.get('name') == task_name:
                if task.get('executor') == user_name:
                    return task
                else:
                    raise exceptions.Error()
        return None

    def load_all_tasks(self):

        # load all tasks from json-file
        with open(os.path.join(self.task_path, 'tasks.json'), 'r') as outfile:
            tasks = json.load(outfile)

        return tasks

    def load_reminder_by_name(self, user_name, reminder_name):

        # load all reminders from json-file by name with check of current user
        with open(os.path.join(self.reminder_path, 'reminders.json'), 'r') as outfile:
            reminders = json.load(outfile)

        for reminder in reminders:
            if reminder.get('name') == reminder_name:
                if reminder.get('executor') == user_name:
                    return reminder
                else:
                    raise exceptions.Error()
        return None

    def load_all_reminders(self):

        # load all reminders from json-file
        with open(os.path.join(self.reminder_path, 'reminders.json'), 'r') as outfile:
            reminders = json.load(outfile)

        return reminders

    def load_user_by_name(self, user_name, user_password):

        # load all user from json-file by name with check of current user
        with open(os.path.join(self.user_path, 'users.json'), 'r') as outfile:
            users = json.load(outfile)

        # juxtaposition user_name and name in file
        for user in users:
            if user.get('name') == user_name:
                if user.get('password') == user_password:
                    return user
                else:
                    raise exceptions.Error()
        return None

    def load_all_users(self):

        # load all user from json-file
        with open(os.path.join(self.user_path, 'users.json'), 'r') as outfile:
            users = json.load(outfile)

        return users

    def add_task(self, item):

        # load all tasks from json-file
        with open(os.path.join(self.task_path, 'tasks.json'), 'r') as outfile:
            tasks = json.load(outfile)

        # add new task
        tasks.append(item)

        # and write it
        self.save_tasks(tasks)


    def add_reminder(self, item):

        # load all reminders from json-file
        with open(os.path.join(self.reminder_path, 'reminders.json'), 'r') as outfile:
            reminders = json.load(outfile)

        # add new reminder
            reminders.append(item)

        # and write it
        self.save_reminders(reminders)


    def add_user(self, item):

        # load all users from json-file
        with open(os.path.join(self.user_path, 'users.json'), 'r') as outfile:
            users = json.load(outfile)

        # add new user
        users.append(item)

        # and write it
        self.save_users(users)


    def save_tasks(self, content):

        # save tasks in the json-file
        with open(os.path.join(self.task_path, 'tasks.json'), 'w') as outfile:
            json.dump(content, outfile, indent=2, sort_keys=True)


    def save_reminders(self, content):

        # save reminders in the json-file
        with open(os.path.join(self.reminder_path, 'reminders.json'), 'w') as outfile:
            json.dump(content, outfile, indent=2, sort_keys=True)


    def save_users(self, content):

        # save users in the json-file
        with open(os.path.join(self.user_path, 'users.json'), 'w') as outfile:
            json.dump(content, outfile, indent=2, sort_keys=True)


    def edit_task(self, content):

        # load all tasks from json-file
        with open(os.path.join(self.task_path, 'tasks.json'), 'r') as outfile:
            tasks = json.load(outfile)

        # searching for a necessary task by name
        for i, task in enumerate(tasks):
            if task.get('name') == content.get('name'):
                tasks[i].update(content)

        # save updated list of tasks
        self.save_tasks(tasks)


    def edit_reminder(self, content):

        # load all reminders from json-file
        with open(os.path.join(self.reminder_path, 'reminders.json'), 'r') as outfile:
            reminders = json.load(outfile)

        # searching for a necessary reminder by name
        for i, reminder in enumerate(reminders):
            if reminder.get('name') == content.get('name'):
                reminders[i].update(content)

        # save updated list of reminders
        self.save_reminders(reminders)


    def delete_task(self, content):

        # load all tasks from json-file
        with open(os.path.join(self.task_path, 'tasks.json'), 'r') as outfile:
            tasks = json.load(outfile)

        # pop task for the list
        tasks.remove(content)

        # save updated list of tasks
        self.save_tasks(tasks)


    def delete_reminder(self, content):

        # load all reminders from json-file
        with open(os.path.join(self.reminder_path, 'reminders.json'), 'r') as outfile:
            reminders = json.load(outfile)

        # pop reminder for the list
        reminders.remove(content)

        # save updated list of reminders
        self.save_reminders(reminders)
